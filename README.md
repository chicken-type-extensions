chicken-type-extensions
=======================
Miscellaneous extensions for the CHICKEN type system.

Description
-----------
This extension adds support for complex type abbreviations to CHICKEN's
native [`define-type`][define-type] form.

    $ cat pair-of.scm
    (define-type (pair-of a) (pair a a))
    
    (: pair (forall (a) (a -> (pair-of a))))
    (define (pair x) (cons x x))
    
    (compiler-typecase (pair 1)
      ((pair-of fixnum)
       (print '(pair-of fixnum)))
      (else
       (print 'else)))
    
    $ csc -X type-extensions pair-of.scm
    $ ./pair-of
    (pair-of fixnum)

[define-type]: http://api.call-cc.org/doc/chicken/types#def:define-type

Requirements
------------
  * [matchable](http://wiki.call-cc.org/egg/matchable)

Installation
------------
    $ git clone http://git.foldling.org/chicken-type-extensions.git
    $ cd chicken-type-extensions
    $ chicken-install

Usage
-----
Load `type-extensions` as a compiler extension with the `-extend` flag
to `csc`(1):

    $ csc -extend type-extensions <file>

Documentation is available at [chickadee][] and on the [CHICKEN
wiki][wiki].

[chickadee]: http://api.call-cc.org/doc/type-extensions
[wiki]: http://wiki.call-cc.org/egg/type-extensions

Author
------
Evan Hanson <evhan@foldling.org>

License
-------
BSD. See LICENSE for details.
